import firebase from 'firebase'
import firestore from 'firebase/firestore'

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyBOfL4oUWHoaDtLUeITXlEbIqNrLAbiZ6A",
  authDomain: "petalia-cc61b.firebaseapp.com",
  databaseURL: "https://petalia-cc61b.firebaseio.com",
  projectId: "petalia-cc61b",
  storageBucket: "petalia-cc61b.appspot.com",
  messagingSenderId: "108601974293",
  appId: "1:108601974293:web:8c4bfbee21ba925939b81c"
};
  // Initialize Firebase
  const firebaseApp = firebase.initializeApp(firebaseConfig);

  //export firestore DB

  export default firebaseApp.firestore()