const express = require('express')
const moment = require('moment')
const db = require('./firebase/init')
const app = express()
const port = process.env.PORT || 3001

app.use(express.json())

var mqtt = require('mqtt');

var options = {
    port: 13142,
    host: 'mqtt://m23.cloudmqtt.com',
    clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8),
    username: 'pewygeeb',
    password: 'I3aMlpYtj3zB',
    keepalive: 60,
    reconnectPeriod: 1000,
    protocolId: 'MQIsdp',
    protocolVersion: 3,
    clean: true,
    encoding: 'utf8'
};
var client = mqtt.connect('mqtt://m23.cloudmqtt.com', options);
client.on('connect', function() { // When connected
    console.log('connected');
    // subscribe to a topic
    client.subscribe('+', function() {
        // when a message arrives, do something with it
        client.on('message', function(topic, message, packet) {
            console.log("Received '" + message + "' on '" + topic + "'" + "at" + moment(Date.now()).format('DD-MM-YYYY HH:mm:ss'));
            const data = JSON.parse(message)
            db.collection(topic).add({
                title: data.device,
                temperature: data.temperature,
                timestamp: Date.now()
            })
        });
    });

    // // publish a message to a topic
    // client.publish('topic1/#', 'my message', function() {
    //     console.log("Message is published");
    //     client.end(); // Close the connection when published
    // });
});

app.listen(port , () => {
    console.log('Server is up on portt ' + port)
  })